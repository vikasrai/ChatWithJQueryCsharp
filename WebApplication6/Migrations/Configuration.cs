namespace WebApplication6.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebApplication6.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WebApplication6.Models.context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(WebApplication6.Models.context context)
        {
            //  This method will be called after migrating to the latest version.
            IList<Users> Users = new List<Users>();

            Users.Add(new Users() { id = 1, name = "aa1", username = "u1" });
            Users.Add(new Users() { id = 2, name = "aa2", username = "u2" });
            Users.Add(new Users() { id = 3, name = "aa3", username = "u3" });

            foreach (Users std in Users)
                context.User.AddOrUpdate(std);
            context.SaveChanges();

            IList<Chaters> Chaters = new List<Chaters>();
            Chaters.Add(new Chaters() { Id = "vikas", name = "vikas" });
            Chaters.Add(new Chaters() { Id = "Akash", name = "Akash" });
            Chaters.Add(new Chaters() { Id = "General", name = "General" });

            foreach (Chaters std in Chaters)
                context.Chaters.AddOrUpdate(std);

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
