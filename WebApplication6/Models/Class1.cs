﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class Users
    {
        public string name { get; set; }
        public string username { get; set; }
        public int id { get; set; }
        public string vikas { get; set; }
        public string vikasdd { get; set; }
    }
    public class Chaters
    {
        public string Id { get; set; }
        public string name { get; set; }
    }
    public class Chat
    {
        public int Id { get; set; }
        public string User { get; set; }
        public string Message { get; set; }
        public string massegeFor { get; set; }
        public string massegeDate { get; set; }

    }

    public class Users1
    {
        public string name { get; set; }
        public string username { get; set; }
        public int id { get; set; }
        public string idd { get; set; }
        public string iddv { get; set; }
    }
    public class context : DbContext
    {
        public context()
        {
           // Database.SetInitializer(new SchoolDBInitializer());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<context, WebApplication6.Migrations.Configuration>());
        }
        public DbSet<Users> User { get; set; }
        public DbSet<Users1> User1 { get; set; }
        public DbSet<Chat> Chat { get; set; }
        public DbSet<Chaters> Chaters { get; set; }
    }
    public class SchoolDBInitializer : DropCreateDatabaseIfModelChanges<context>
    {
       // MigrateDatabaseToLatestVersion<context, WebApplication6.Migrations.Configuration> a = new MigrateDatabaseToLatestVersion<context, WebApplication6.Migrations.Configuration>();
        protected override void Seed(context context)
        {
            IList<Users> Users = new List<Users>();

            Users.Add(new Users() { id = 1, name = "aa1", username = "u1" });
            Users.Add(new Users() { id = 2, name = "aa2", username = "u2" });
            Users.Add(new Users() { id = 3, name = "aa3", username = "u3" });

            foreach (Users std in Users)
                context.User.Add(std);

            

            base.Seed(context);
        }
    }

}