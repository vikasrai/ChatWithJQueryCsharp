﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Models;
using System.Data.Entity.Migrations;
namespace WebApplication6.Controllers
{
    public class HomeController : Controller
    {
        context c = new context();
        public ActionResult Index()
        {
            var a = c.User.ToList();
            // var p = c.Chaters.ToList();
            IList<Users1> Users1 = new List<Users1>();

            Users1.Add(new Users1() { id = 1 });
            Users1.Add(new Users1() { id = 2, name = "aa2", username = "u2" });
            Users1.Add(new Users1() { id = 3, name = "aa3", username = "u3" });


            foreach (Users1 std in Users1)
                c.User1.Add(std);
            c.SaveChanges();
            var a1 = c.User1.ToList();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Login()
        {

            return View();
        }
        public ActionResult LogOut()
        {
            Session["id"] = null;
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult Login(string name)
        {
            c.Chaters.AddOrUpdate(new Chaters() { Id = name, name = name });
            c.SaveChanges();
            Session["id"] = name;

            return RedirectToAction("Chat");
        }
        public ActionResult Chat()
        {
            if (Session["id"] == null)
                return RedirectToAction("Login");
            return View();
        }
        public ActionResult getChats(string user, string msgFor)
        {
            if (user == "")
                return RedirectToAction("Login");
            List<object> obj = new List<object>();
            obj.Add(c.Chaters.ToList());
            List<Chat> chats = new List<Chat>();
            if (msgFor == "General")
                chats = (from chat in c.Chat where (chat.massegeFor == msgFor) select chat).OrderBy(x => x.massegeDate).ToList();
            else
                chats = (from chat in c.Chat where ((chat.massegeFor == user && chat.User == msgFor) || (chat.User == user && chat.massegeFor == msgFor)) select chat).OrderBy(x => x.massegeDate).ToList();
            obj.Add(chats);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveChats(string user, string msg, string msgFor)
        {
            if (msgFor == "")
                return Json("select user first", JsonRequestBehavior.AllowGet);
            if (user == "")
                return RedirectToAction("Login");
            c.Chat.Add(new Chat() { User = user, massegeFor = msgFor, Message = msg, massegeDate = DateTime.Now.ToString("hh:mm:ss tt dd/MM/yyyy") });
            c.SaveChanges();
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}